import {Component} from "@angular/core";
import {NavController, MenuController} from "ionic-angular";
import {LoginPage} from "../login/login";
import {RegisterPage} from "../register/register";


@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})


export class AboutPage {
  constructor(public nav: NavController, public menu: MenuController) {
    this.menu.swipeEnable(false);
  }

  login(){
    this.nav.setRoot(LoginPage);
  }

  register(){
    this.nav.setRoot(RegisterPage);
  }
}
