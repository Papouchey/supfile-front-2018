import {Component} from "@angular/core";
import {NavController, MenuController, ToastController, PopoverController, NavParams} from "ionic-angular";
import {FormBuilder} from "@angular/forms";
import {Http, Headers, RequestOptions} from "@angular/http";
import * as Constants from '../constants';
import {HomePage} from '../home/home';


@Component({
  selector: 'page-move',
  templateUrl: 'move.html'
})

export class MovePage {

  public token = sessionStorage.getItem('token');
  public id_user = sessionStorage.getItem('id_user');
  public username = '/' + sessionStorage.getItem('username') + '/';
  public path = sessionStorage.getItem('path');
  public path_exploded;
  public path_to_show = [];
  public name = this.navParams.data.name;

  public documents = [];

  constructor(public nav: NavController, public menu: MenuController, private formBuilder: FormBuilder, public http: Http, public toastCtrl: ToastController,
              public popoverCtrl: PopoverController, private navParams: NavParams) {

    let oldPath = sessionStorage.getItem('path');
    this.path_exploded = oldPath.split('/');
    this.path_exploded.pop();
    let actual_path = '',
      level = 0;
    for (var i=0;i<this.path_exploded.length;i++) {
      actual_path += this.path_exploded[i]+'/';
      this.path_to_show.push({
        name: this.path_exploded[i],
        path: actual_path,
        level: level
      });
      level+=1;
    }
  }

  ionViewWillEnter(){
    var url = Constants.PROTO_API + Constants.DOMAIN_API + 'get-foldersExcept/?token='
      + sessionStorage.getItem('token')
      + '&id_user=' + sessionStorage.getItem('id_user')
      + '&name=' + this.navParams.data.name
      + '&path=' + this.path;
    this.http.get(url)
      .subscribe(data => {
        this.documents = JSON.parse(data.text());
      }, error => {
      });
  }


  moveFile(newPath){
    let toast = this.toastCtrl.create({
      message: '',
      duration: 3000,
      position: 'bottom',
      closeButtonText: '×',
      showCloseButton: true,
      cssClass: "error-notify",
    });


    let headers = new Headers({'Content-Type': 'application/json',  'Access-Control-Allow-Origin': '*'});
    let options = new RequestOptions({headers: headers});
    let old = this.name.split('.');
    let level = newPath.split('/');
    let body = JSON.stringify({
      token: this.token,
      id_user: this.id_user,
      name: old[0],
      extension: '.' + old[1],
      oldPath: this.path,
      newPath: newPath,
      level: level.length-2
    });

    this.http.put(Constants.PROTO_API+Constants.DOMAIN_API+'moveFile/'+this.name, body, options)
      .subscribe(data => {
        if (data.json().response == 'Oki'){
          this.nav.setRoot(HomePage);
        } else {
          toast.setMessage(data.json().response);
          toast.present();
          return;
        }
      });
  }
}
