import {Component, ViewChild} from "@angular/core";
import {NavController, NavParams, PopoverController, List, ToastController, Events} from "ionic-angular";
import { Chart } from 'chart.js';

import {NotificationsPage} from "../notifications/notifications";
import {PreviewPage} from "../preview/preview";
import {CreateDirPage} from "../create-dir/create-dir";
import {SearchPage} from "../search/search";

import * as Constants from "../constants";
import { Headers, Http, RequestOptions } from "@angular/http";
import { FormBuilder, FormGroup } from "@angular/forms";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
    @ViewChild(List) list: List;

    private upload_form: FormGroup;
    doughnutChart: any;
    public images_ext;
    public videos_ext;
    public audio_ext;
    public doc_ext;
    public path_exploded;
    public path_to_show = [];

    public search = {
        name: "Find file/directory"
    };

    public documents = [];

    constructor(public nav: NavController, public popoverCtrl: PopoverController, public http: Http
        , private formBuilder: FormBuilder, public toastCtrl: ToastController, private navParams: NavParams, public events: Events) {

        this.upload_form = this.formBuilder.group({
          file: [''],
        });

        if (navParams.get('path') !== undefined && navParams.get('level') !== undefined) {
          sessionStorage.setItem('path', navParams.get('path'));
          sessionStorage.setItem('level', navParams.get('level'));
        }

        let oldPath = sessionStorage.getItem('path');
        this.path_exploded = oldPath.split('/');
        this.path_exploded.pop();
        let actual_path = '',
          level = 0;
        for (var i=0;i<this.path_exploded.length;i++) {
          actual_path += this.path_exploded[i]+'/';
          this.path_to_show.push({
            name: this.path_exploded[i],
            path: actual_path,
            level: level
          });
          level+=1;
        }

        if (navParams.get('path') !== undefined && navParams.get('level') !== undefined) {
          this.openFolder(this.path_to_show[this.path_to_show.length - 1].name, this.path_to_show[this.path_to_show.length - 1].level, this.path_to_show[this.path_to_show.length - 1].path);
        }

        this.events.publish('user:logged');
    }


    ionViewWillEnter() {
        this.images_ext = Constants.IMAGE_EXT;
        this.videos_ext = Constants.VIDEO_EXT;
        this.audio_ext = Constants.AUDIO_EXT;
        this.doc_ext = Constants.DOC_EXT;

        var url = Constants.PROTO_API + Constants.DOMAIN_API + 'get-documents/?token='
            + sessionStorage.getItem('token')
            + '&id_user=' + sessionStorage.getItem('id_user') + '&level='
            + sessionStorage.getItem('level')
            + '&path=' + sessionStorage.getItem('path');
        this.http.get(url)
            .subscribe(data => {
                this.documents = JSON.parse(data.text());
            }, error => {
            });
    }

    // to go account page
    preview($event) {
      let file = $event.target.innerHTML;
      let extension = file.split('.').pop();

      if (this.images_ext.indexOf(extension.toLowerCase()) == -1 && this.videos_ext.indexOf(extension.toLowerCase()) == -1 && this.audio_ext.indexOf(extension.toLowerCase()) == -1&& this.doc_ext.indexOf(extension.toLowerCase()) == -1) {
        let toast = this.toastCtrl.create({
          message: 'Impossible preview',
          duration: 3000,
          position: 'bottom',
          closeButtonText: '×',
          showCloseButton: true,
          cssClass: "error-notify",
        });
        toast.present();
        return;
      } else {
        this.nav.push(PreviewPage, {myEvent: $event});
      }
    }

    presentNotifications(name, IsPublic) {
      let popover = this.popoverCtrl.create(NotificationsPage, {
        name: name,
        isPublic: IsPublic,
      });
      popover.present({
        ev: name,
      });
    }

    async upload($event) {
      var formData = new FormData();
      var file = $event.target.files[0];

      let headers = new Headers({'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'});
      let options = new RequestOptions({headers: headers});
      let toast = this.toastCtrl.create({
          message: '',
          duration: 3000,
          position: 'bottom',
          closeButtonText: '×',
          showCloseButton: true,
          cssClass: "error-notify",
      });

      this.http.get(Constants.PROTO_API + Constants.DOMAIN_API + 'directory/' + sessionStorage.getItem('username') + '/?token=' + sessionStorage.getItem('token') + '&id_user=' + sessionStorage.getItem('id_user'), options)
          .subscribe(data => {
              var sizeTotalGo = 30;
              var sizeUsedGo = data.json().size / 1024;
              var sizeRemainingGo = sizeTotalGo - sizeUsedGo;

              if (sizeRemainingGo < file.size / 1024 / 1024 / 1024) {
                  toast.setMessage('Not enough space !');
                  toast.present();
                  return;
              } else {
                  formData.append('file', file, file.name);
                  formData.append('id_user', sessionStorage.getItem('id_user'));
                  formData.append('path', sessionStorage.getItem('path'));
                  formData.append('level', sessionStorage.getItem('level'));
                  formData.append('token', sessionStorage.getItem('token'));
                  formData.append('is_public', '0');
                  formData.append('is_folder', '0');
                  this.http.post(Constants.PROTO_API + Constants.DOMAIN_API + 'upload', formData, {})
                      .subscribe(data => {
                        if (data.text() == "upload"){
                          this.nav.setRoot(HomePage);
                        } else {
                          toast.setMessage('File already exist !');
                          toast.present();
                          return;
                        }
                      }, error => {
                      });
              }
          });
      }

    //Ouvre la page de création de dossier
    async openCreateDir($event) {
        this.nav.push(CreateDirPage, {myEvent: $event});
    }

    //Met à jour les informations pour ouvrir un dossier
    openFolder(name, level, path = null) {
        var oldPath = sessionStorage.getItem('path');
        if (path != null) {
            sessionStorage.setItem('path', path);
        } else {
            sessionStorage.setItem('path', oldPath+name+'/');
        }
        sessionStorage.setItem('level', level);

        this.nav.setRoot(HomePage);
    }

    searchNav() {
      this.nav.push(SearchPage);
    }
}
