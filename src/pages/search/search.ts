import {Component, ViewChild, ElementRef} from "@angular/core";
import {NavController, MenuController, ToastController, PopoverController} from "ionic-angular";
import {Http, Headers, RequestOptions} from "@angular/http";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HomePage} from "../home/home";
import * as Constants from '../constants';


@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})


export class SearchPage {
  private search_form : FormGroup;
  @ViewChild('name', {read: ElementRef}) name: ElementRef;

  public documents = [];

  public images_ext = Constants.IMAGE_EXT;
  public videos_ext = Constants.VIDEO_EXT;
  public audio_ext = Constants.AUDIO_EXT;

  constructor(public nav: NavController, public menu: MenuController, private formBuilder: FormBuilder, public http: Http, public toastCtrl: ToastController,
              public popoverCtrl: PopoverController) {
    this.search_form = this.formBuilder.group({
      name: ['', Validators.required],
    });
  }


  searchNav() {
    let toast = this.toastCtrl.create({
      message: '',
      duration: 3000,
      position: 'bottom',
      closeButtonText: '×',
      showCloseButton: true,
      cssClass: "error-notify",
    });

    let characters = /^[a-zA-Z0-9 _-àâäéèêëïîôöùûüÿçÀÂÄÇÉÈÊËÎÏÔÖÙÛÜŸ]*$/;
    if (this.search_form.value.name == '' || characters.test(this.search_form.value.name) === false) {
      this.addElementInvalid(this.name);
      toast.setMessage('Enter a valid name');
      toast.present();
    } else {
      this.http.get(Constants.PROTO_API + Constants.DOMAIN_API + 'search/?id_user=' + sessionStorage.getItem('id_user') + '&token=' + sessionStorage.getItem('token') + '&name=' + this.search_form.value.name)
        .subscribe(data => {
            this.documents = data.json().result;
        });
    }
  }

  completeSearch(newPath) {
    let path;
    if(newPath.is_folder == 0) {
      path = newPath.path.substr(1, newPath.path.length);
    } else if (newPath.is_folder == 1) {
      path = newPath.path.substr(1, newPath.path.length) + newPath.name + '/';
    }
    this.nav.setRoot(HomePage, {path: path, level: newPath.level});
  }

  addElementInvalid(element) {
    element.nativeElement.classList.add("ng-invalid");
    element.nativeElement.classList.add("ng-touched");
    element.nativeElement.classList.remove("ng-valid");
  }
}
