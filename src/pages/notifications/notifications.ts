import {Component, ViewChild, ElementRef} from "@angular/core";
import {NavParams, ViewController, NavController} from "ionic-angular";
import * as Constants from "../constants";
import {Http, RequestOptions, Headers} from "@angular/http";
import {FormBuilder,FormGroup} from "@angular/forms";
import {RenamePage} from "../rename/rename";
import {MovePage} from "../move/move";
import * as Clipboard from 'clipboard/dist/clipboard.min.js';

@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html'
})

export class NotificationsPage {
    private is_public_form : FormGroup;
    @ViewChild('is_public', {read: ElementRef}) is_public: ElementRef;

    public domain_api = Constants.DOMAIN_API;
    public token = sessionStorage.getItem('token');
    public id_user = sessionStorage.getItem('id_user');
    public path = sessionStorage.getItem('path');
    public name;
    public downloadLinkPrivate;
    public IsCheck;
    public downloadLink2;
    public clipboard;

    constructor(public viewCtrl: ViewController, public http: Http, private navParams: NavParams, public nav: NavController,
                private formBuilder: FormBuilder) {
      this.is_public_form = this.formBuilder.group({
        is_public: [''],
        downloadLink: [''],
      });
      this.clipboard = new Clipboard('#cpyBtn');
    }

    close() {
        this.viewCtrl.dismiss();
    }

    ngOnInit() {
        if (this.navParams.data) {
            this.name = this.navParams.data.name;

            this.downloadLinkPrivate = 'http://'+this.domain_api+'download/'+this.name+'/?token='+this.token+'&id_user='+this.id_user+'&path='+this.path;
            if (this.navParams.data.isPublic == 0) {
              this.IsCheck = false;
            } else if (this.navParams.data.isPublic == 1) {
              this.IsCheck = true;

              let headers = new Headers({ 'Content-Type': 'application/json',  'Access-Control-Allow-Origin': '*'  });
              let options = new RequestOptions({ headers: headers });
              this.http.get(Constants.PROTO_API+Constants.DOMAIN_API+'IsPublicFile/'+this.name+'/?token=' + sessionStorage.getItem('token') + '&id_user=' + sessionStorage.getItem('id_user') + '&path=' + sessionStorage.getItem('path'), options)
                .subscribe(data =>{
                  this.downloadLink2 = Constants.DOMAIN_API + 'download/' + data.json().username + '/' + data.json().token_public;
                });
            }
        }
    }

    removeFile() {
        let headers = new Headers({ 'Content-Type': 'application/json',  'Access-Control-Allow-Origin': '*'  });
        let options = new RequestOptions({ headers: headers,
            params: {
             token: this.token,
             path: this.path,
             id_user: this.id_user
            }
        });
        this.http.delete(Constants.PROTO_API+Constants.DOMAIN_API+'file/'+this.name, options)
            .subscribe(data => {
                location.reload();
            });
    }

    renameFile(name) {
      this.nav.push(RenamePage, {name: name});
    }

    moveFile(name) {
      this.nav.push(MovePage, {name: name});
    }

    openWindowDownload() {
        window.open(this.downloadLinkPrivate, '_system', 'location=yes');
    }

    isPublicCheck($event) {
      var is_pub = 0;
      if (this.is_public_form.value.is_public !== false) {
        is_pub = 1;
      }
      let body = JSON.stringify({
        name: this.name,
        path: sessionStorage.getItem('path'),
        id_user: sessionStorage.getItem('id_user'),
        token: sessionStorage.getItem('token'),
        level: sessionStorage.getItem('level'),
        is_public: is_pub
      });

      let headers = new Headers({ 'Content-Type': 'application/json',  'Access-Control-Allow-Origin': '*'  });
      let options = new RequestOptions({ headers: headers });
      this.http.put(Constants.PROTO_API+Constants.DOMAIN_API+'IsPublicFile/'+this.name, body, options)
        .subscribe(data =>{
          location.reload();
        });
    }
}

