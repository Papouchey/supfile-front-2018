export const DOMAIN_API = "localhost:3000/api/";
export const DOMAIN = "localhost:8100/";
export const PROTO = "http://";
export const PROTO_API = "http://";
export const IMAGE_EXT = ['png', 'jpg', 'gif', 'jpeg', 'bmp'];
export const VIDEO_EXT = ['mp4', 'webm', 'ogv'];
export const AUDIO_EXT = ['wab', 'mp3', 'ogg', 'flac'];
export const DOC_EXT = ['txt'];
