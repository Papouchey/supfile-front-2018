import {Component, ViewChild, ElementRef} from "@angular/core";
import {NavController, MenuController, ToastController, PopoverController} from "ionic-angular";
import {Http, Headers, RequestOptions} from "@angular/http";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HomePage} from "../home/home";
import {PreviewPage} from "../preview/preview";
import {NotificationsPage} from "../notifications/notifications";
import * as Constants from '../constants';


@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})


export class EditProfilePage {
  private edit_profile_form : FormGroup;
  @ViewChild('firstname', {read: ElementRef}) firstname: ElementRef;
  @ViewChild('lastname', {read: ElementRef}) lastname: ElementRef;
  @ViewChild('email', {read: ElementRef}) email: ElementRef;
  @ViewChild('password', {read: ElementRef}) password: ElementRef;
  @ViewChild('password_confirmation', {read: ElementRef}) password_confirmation: ElementRef;

  constructor(public nav: NavController, public menu: MenuController, private formBuilder: FormBuilder, public http: Http, public toastCtrl: ToastController,
              public popoverCtrl: PopoverController) {
    this.edit_profile_form = this.formBuilder.group({
      firstname: [''],
      lastname: [''],
      email: ['', Validators.compose([Validators.email, Validators.required])],
      password: [''],
      password_confirmation: ['']
    });

    this.menu.close();

    this.http.get(Constants.PROTO_API + Constants.DOMAIN_API + 'user/?id_user='+sessionStorage.getItem('id_user'))
      .subscribe(data => {
        this.edit_profile_form = this.formBuilder.group({
          firstname: [data.json().firstname],
          lastname: [data.json().lastname],
          email: [data.json().email, Validators.compose([Validators.email, Validators.required])],
          password: [''],
          password_confirmation: ['']
        });
      }, error => {
        console.log("Oooops!");
        return;
      });
  }


  saveEditProfile(){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    let toast = this.toastCtrl.create({
      message: "",
      duration: 3000,
      position: 'bottom',
      closeButtonText: 'x',
      showCloseButton: true,
      cssClass: 'error-notify',
    });
    if (this.edit_profile_form.value.firstname == '' || this.edit_profile_form.value.lastname == '' || this.edit_profile_form.value.email == '') {
      toast.setMessage('Fill all the fields');
      toast.present();
      if (this.edit_profile_form.value.firstname == '') {
        this.addElementInvalid(this.firstname);
      }
      if (this.edit_profile_form.value.lastname == '') {
        this.addElementInvalid(this.lastname);
      }
      if (this.edit_profile_form.value.email == '') {
        this.addElementInvalid(this.email);
      }
      return;
    }

    let characters = /^[a-zA-Z0-9 _-àâäéèêëïîôöùûüÿçÀÂÄÇÉÈÊËÎÏÔÖÙÛÜŸ]*$/;
    let resultFirstname = characters.test(this.edit_profile_form.value.firstname);
    let resultLastname = characters.test(this.edit_profile_form.value.lastname);
    if (resultFirstname === false){
      toast.setMessage("Invalid firstname (no special characters)");
      toast.present();
      this.addElementInvalid(this.firstname);
      return;
    }
    if (resultLastname === false){
      toast.setMessage("Invalid lastname (no special characters)");
      toast.present();
      this.addElementInvalid(this.lastname);
      return;
    }

    let mail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let result2 = mail.test(this.edit_profile_form.value.email);
    if (result2 === false) {
      toast.setMessage("Invalid Email");
      toast.present();
      this.addElementInvalid(this.email);
      return;
    }

    let body;
    if (this.edit_profile_form.value.password !== '' || this.edit_profile_form.value.password_confirmation !== ''){
      if (this.edit_profile_form.value.password_confirmation !== this.edit_profile_form.value.password) {
        toast.setMessage("Passwords don't match");
        toast.present();
        if (this.edit_profile_form.value.password == '') {
          this.addElementInvalid(this.password);
        }
        if (this.edit_profile_form.value.password_confirmation == '') {
          this.addElementInvalid(this.password_confirmation);
        }
        return;
      } else {
        body = JSON.stringify({
          id: sessionStorage.getItem('id_user'),
          firstname: this.edit_profile_form.value.firstname,
          lastname: this.edit_profile_form.value.lastname,
          email: this.edit_profile_form.value.email,
          password: this.edit_profile_form.value.password
        });
      }
    } else {
      body = JSON.stringify({
        id: sessionStorage.getItem('id_user'),
        firstname: this.edit_profile_form.value.firstname,
        lastname: this.edit_profile_form.value.lastname,
        email: this.edit_profile_form.value.email,
        password: ''
      });
    }

    //Send request to API to register
    this.http.put(Constants.PROTO_API + Constants.DOMAIN_API + 'user', body, options)
      .subscribe(data => {
        if (data.json().response == 'Email already taken'){
          toast.setMessage("Email already taken");
          toast.present();
          this.addElementInvalid(this.email);
          return;
        } else {
          sessionStorage.setItem("firstname",data.json().firstname);
          sessionStorage.setItem("lastname",data.json().lastname);
          sessionStorage.setItem("email",data.json().email);

          document.getElementById('name').innerHTML = sessionStorage.getItem('firstname') + ' ' + sessionStorage.getItem('lastname');
          document.getElementById('username').innerHTML = sessionStorage.getItem('username');

          this.nav.setRoot(HomePage);
        }
      }, error => {
        console.log("Oooops!");
        return;
      });
  }


  addElementInvalid(element) {
    element.nativeElement.classList.add("ng-invalid");
    element.nativeElement.classList.add("ng-touched");
    element.nativeElement.classList.remove("ng-valid");
  }

  // to go account page
  goToAccount() {
    this.nav.push(PreviewPage);
  }


  presentNotifications(myEvent) {
    console.log(myEvent);
    let popover = this.popoverCtrl.create(NotificationsPage);
    popover.present({
      ev: myEvent
    });
  }
}
