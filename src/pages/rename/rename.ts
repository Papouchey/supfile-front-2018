import {Component, ElementRef, ViewChild} from "@angular/core";
import {NavController, MenuController, ToastController, PopoverController, NavParams} from "ionic-angular";
import {FormGroup, FormBuilder} from "@angular/forms";
import {Http, Headers, RequestOptions} from "@angular/http";
import * as Constants from '../constants';
import {HomePage} from "../home/home";


@Component({
  selector: 'page-rename',
  templateUrl: 'rename.html'
})

export class RenamePage {
  private rename_form : FormGroup;
  @ViewChild('new_name', {read: ElementRef}) new_name: ElementRef;

  public token = sessionStorage.getItem('token');
  public id_user = sessionStorage.getItem('id_user');
  public path = sessionStorage.getItem('path');
  public old_fileName = this.navParams.data.name;


  constructor(public nav: NavController, public menu: MenuController, private formBuilder: FormBuilder, public http: Http, public toastCtrl: ToastController,
              public popoverCtrl: PopoverController, private navParams: NavParams) {
    this.rename_form = this.formBuilder.group({
      new_name: ['']
    });
  }

  renameFile() {
    let toast = this.toastCtrl.create({
      message: '',
      duration: 3000,
      position: 'bottom',
      closeButtonText: '×',
      showCloseButton: true,
      cssClass: "error-notify",
    });

    let characters = /^[a-zA-Z0-9 _-àâäéèêëïîôöùûüÿçÀÂÄÇÉÈÊËÎÏÔÖÙÛÜŸ]*$/;
    if (this.rename_form.value.new_name == '' || characters.test(this.rename_form.value.new_name) === false) {
      toast.setMessage('Enter a valid name');
      toast.present();
      this.addElementInvalid(this.new_name);
    } else {
      let headers = new Headers({'Content-Type': 'application/json',  'Access-Control-Allow-Origin': '*'});
      let options = new RequestOptions({headers: headers});
      let old = this.old_fileName.split('.');
      let body = JSON.stringify({
        token: this.token,
        path: this.path,
        id_user: this.id_user,
        old_fileName: old[0],
        extension: '.' + old[1],
        new_fileName: this.rename_form.value.new_name
      });

      this.http.put(Constants.PROTO_API+Constants.DOMAIN_API+'file/'+this.old_fileName, body, options)
        .subscribe(data => {
          if (data.json().response == 'Oki'){
            this.nav.setRoot(HomePage);
          } else {
            toast.setMessage(data.json().response);
            toast.present();
            return;
          }
        });
    }
  }

  addElementInvalid(element) {
    element.nativeElement.classList.add("ng-invalid");
    element.nativeElement.classList.add("ng-touched");
    element.nativeElement.classList.remove("ng-valid");
  }
}
