import {Component, ElementRef, ViewChild} from "@angular/core";
import {NavController, ToastController} from "ionic-angular";
import {LoginPage} from "../login/login";
import * as Constants from '../constants';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Http, Headers, RequestOptions} from '@angular/http';


@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})


export class RegisterPage {
  private register_form: FormGroup;
  @ViewChild('firstname', {read: ElementRef}) firstname: ElementRef;
  @ViewChild('lastname', {read: ElementRef}) lastname: ElementRef;
  @ViewChild('username', {read: ElementRef}) username: ElementRef;
  @ViewChild('email', {read: ElementRef}) email: ElementRef;
  @ViewChild('password', {read: ElementRef}) password: ElementRef;
  @ViewChild('password_confirmation', {read: ElementRef}) password_confirmation: ElementRef;

  constructor(public nav: NavController, public http: Http, private formBuilder: FormBuilder, public toastCtrl: ToastController) {
    this.register_form = this.formBuilder.group({
      firstname: [''],
      lastname: [''],
      username: [''],
      email: ['', Validators.compose([Validators.email, Validators.required])],
      password: [''],
      password_confirmation: ['']
    });
  }


  // register and go to home page
  register() {
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    let toast = this.toastCtrl.create({
      message: "",
      duration: 3000,
      position: 'bottom',
      closeButtonText: 'x',
      showCloseButton: true,
      cssClass: 'error-notify',
    });
    if (this.register_form.value.firstname == '' || this.register_form.value.lastname == '' || this.register_form.value.username == '' || this.register_form.value.email == '' || this.register_form.value.password == '' || this.register_form.value.password_confirmation == '') {
      toast.setMessage('Fill all the fields');
      toast.present();
      if (this.register_form.value.firstname == '') {
        this.addElementInvalid(this.firstname);
      }
      if (this.register_form.value.lastname == '') {
        this.addElementInvalid(this.lastname);
      }
      if (this.register_form.value.username == '') {
        this.addElementInvalid(this.username);
      }
      if (this.register_form.value.email == '') {
        this.addElementInvalid(this.email);
      }
      if (this.register_form.value.password == '') {
        this.addElementInvalid(this.password);
      }
      if (this.register_form.value.password_confirmation == '') {
        this.addElementInvalid(this.password_confirmation);
      }
      return;
    }

    let characters = /^[a-zA-Z0-9 _-àâäéèêëïîôöùûüÿçÀÂÄÇÉÈÊËÎÏÔÖÙÛÜŸ]*$/;
    let resultFirstname = characters.test(this.register_form.value.firstname);
    let resultLastname = characters.test(this.register_form.value.lastname);
    let resultUsername = characters.test(this.register_form.value.username);
    if (resultFirstname === false){
      toast.setMessage("Invalid firstname (no special characters)");
      toast.present();
      this.addElementInvalid(this.firstname);
      return;
    }
    if (resultLastname === false){
      toast.setMessage("Invalid lastname (no special characters)");
      toast.present();
      this.addElementInvalid(this.lastname);
      return;
    }
    if (resultUsername === false){
      toast.setMessage("Invalid username (no special characters)");
      toast.present();
      this.addElementInvalid(this.username);
      return;
    }

    let mail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let result2 = mail.test(this.register_form.value.email);
    if (result2 === false) {
      toast.setMessage("Invalid Email");
      toast.present();
      this.addElementInvalid(this.email);
      return;
    }

    if (this.register_form.value.password_confirmation !== this.register_form.value.password) {
      toast.setMessage("Passwords don't match");
      toast.present();
      if (this.register_form.value.password == '') {
        this.addElementInvalid(this.password);
      }
      if (this.register_form.value.password_confirmation == '') {
        this.addElementInvalid(this.password_confirmation);
      }
      return;
    }

    let body = JSON.stringify({
      firstname: this.register_form.value.firstname,
      lastname: this.register_form.value.lastname,
      username: this.register_form.value.username,
      email: this.register_form.value.email,
      password: this.register_form.value.password
    });

    //Send request to API to register
    this.http.post(Constants.PROTO_API + Constants.DOMAIN_API + 'user', body, options)
      .subscribe(data => {
        if (data.text() == '"Username already taken"'){
          toast.setMessage("Username already taken");
          toast.present();
          this.addElementInvalid(this.username);
          return;
        } else if (data.text() == '"Email already taken"'){
          toast.setMessage("Email already taken");
          toast.present();
          this.addElementInvalid(this.email);
          return;
        } else {
          toast.setCssClass("toast-success")
          toast.setMessage("Account created !");
          toast.present();
          this.nav.setRoot(LoginPage);
        }
      }, error => {
        console.log("Oooops!");
        return;
      });
  }


  addElementInvalid(element) {
      element.nativeElement.classList.add("ng-invalid");
      element.nativeElement.classList.add("ng-touched");
      element.nativeElement.classList.remove("ng-valid");
  }


  // go to login page
  login() {
    this.nav.setRoot(LoginPage);
  }
}
