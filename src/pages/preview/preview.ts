import {Component} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import {Http} from "@angular/http";
import * as Constants from "../constants";


@Component({
  selector: 'page-preview',
  templateUrl: 'preview.html'
})
export class PreviewPage {

    public domain_api = Constants.DOMAIN_API;
    public token = sessionStorage.getItem('token');
    public id_user = sessionStorage.getItem('id_user');
    public path = sessionStorage.getItem('path');
    public preview;
    public images_ext = Constants.IMAGE_EXT;
    public videos_ext = Constants.VIDEO_EXT;
    public audio_ext = Constants.AUDIO_EXT;
    public doc_ext = Constants.DOC_EXT;
    public extension;
    public file;

    constructor(public nav: NavController, private navParams: NavParams, public http: Http) {

    }

    ngOnInit() {
        if (this.navParams.data) {
            this.file = this.navParams.data.myEvent.target.innerHTML;
            this.extension = this.file.split('.').pop();

           if(this.doc_ext.indexOf(this.extension.toLowerCase()) === 0) {
              this.http.get(Constants.PROTO_API + Constants.DOMAIN_API + 'getTxt/?id_user=' + sessionStorage.getItem('id_user') + '&token=' + sessionStorage.getItem('token') + '&path=' + this.path + '&name=' + this.file)
                .subscribe(data => {
                    let text = data.json().result;
                    text = text.replace(/\n/g, "<br />");
                    document.getElementById('text').innerHTML = text;
                });
            }
        }
    }
}
