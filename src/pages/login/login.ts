import {Component, ElementRef, ViewChild} from "@angular/core";
import {NavController, AlertController, ToastController, MenuController} from "ionic-angular";
import {HomePage} from "../home/home";
import {RegisterPage} from "../register/register";
import {AboutPage} from "../about/about";
import {Headers, Http, RequestOptions} from "@angular/http";
import {FormBuilder, FormGroup} from "@angular/forms";
import * as Constants from "../constants";


@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})


export class LoginPage {
  private login_form : FormGroup;
  @ViewChild('email', { read: ElementRef }) email: ElementRef;
  @ViewChild('password', { read: ElementRef }) password: ElementRef;


    constructor(public nav: NavController, public forgotCtrl: AlertController, public menu: MenuController,
                public toastCtrl: ToastController, private formBuilder: FormBuilder, public http: Http) {
        if (sessionStorage.getItem('id_user') !== null) {
            this.nav.setRoot(HomePage);
        }
        this.menu.swipeEnable(false);
        this.login_form = this.formBuilder.group({
            email: [''],
            password: [''],
        });
  }


  // go to register page
  register() {
    this.nav.setRoot(RegisterPage);
  }

  about(){
    this.nav.setRoot(AboutPage);
  }


  // login and go to home page
  login() {
      let headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': 'true', 'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT'
      });
      let options = new RequestOptions({ headers: headers });
      let toast = this.toastCtrl.create({
        message: '',
        duration: 3000,
        position: 'bottom',
        closeButtonText: '×',
        showCloseButton: true,
        cssClass: "error-notify",
      });

      if (this.login_form.value.email == '' || this.login_form.value.password == '') {
          toast.setMessage('User/Email or password are incorrect');
          toast.present();
          if (this.login_form.value.email == '') {
            this.addElementInvalid(this.email);
          }
          if (this.login_form.value.password == '') {
            this.addElementInvalid(this.password);
          }
          return;
      }

      let body = JSON.stringify({
          email: this.login_form.value.email,
          password: this.login_form.value.password,
      });
      this.http.post(Constants.PROTO_API+Constants.DOMAIN_API+'login', body, options)
          .subscribe(data => {
              sessionStorage.setItem("id_user",data.json().id);
              sessionStorage.setItem("firstname",data.json().firstname);
              sessionStorage.setItem("lastname",data.json().lastname);
              sessionStorage.setItem("username",data.json().username);
              sessionStorage.setItem("email",data.json().email);
              sessionStorage.setItem("path",data.json().username+"/");
              sessionStorage.setItem("level", "0");
              sessionStorage.setItem("token",data.json().token);

              this.nav.setRoot(HomePage);
          }, error => {
              toast.setMessage('User/Email or password are incorrect');
              toast.present();
              this.addElementInvalid(this.email);
              this.addElementInvalid(this.password);
          });
  }


  addElementInvalid(element) {
    element.nativeElement.classList.add("ng-invalid");
    element.nativeElement.classList.add("ng-touched");
    element.nativeElement.classList.remove("ng-valid");
  }
}
