import {Component, ViewChild, ElementRef} from "@angular/core";
import {NavController, MenuController, ToastController, PopoverController} from "ionic-angular";
import {Http, Headers, RequestOptions} from "@angular/http";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HomePage} from "../home/home";
import * as Constants from '../constants';


@Component({
    selector: 'page-create-dir',
    templateUrl: 'create-dir.html',
})


export class CreateDirPage {
    private create_dir_form : FormGroup;
    @ViewChild('directory_name', {read: ElementRef}) directory_name: ElementRef;
    @ViewChild('is_public', {read: ElementRef}) is_public: ElementRef;

    constructor(public nav: NavController, public menu: MenuController, private formBuilder: FormBuilder, public http: Http, public toastCtrl: ToastController,
                public popoverCtrl: PopoverController) {
        this.create_dir_form = this.formBuilder.group({
            directory_name: ['', Validators.required],
            is_public: ['']
        });
    }


    createSubmit() {
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let toast = this.toastCtrl.create({
          message: '',
          duration: 3000,
          position: 'bottom',
          closeButtonText: '×',
          showCloseButton: true,
          cssClass: "error-notify",
        });

        let characters = /^[a-zA-Z0-9 _-àâäéèêëïîôöùûüÿçÀÂÄÇÉÈÊËÎÏÔÖÙÛÜŸ]*$/;
        if (this.create_dir_form.value.directory_name == '' || characters.test(this.create_dir_form.value.directory_name) === false) {
          this.addElementInvalid(this.directory_name);
          toast.setMessage('Enter a valid directory name');
          toast.present();
        } else {
            var is_pub = 0;
            if (this.create_dir_form.value.is_public !== false) {
                is_pub = 1;
            }
            let body = JSON.stringify({
                name: this.create_dir_form.value.directory_name,
                extension: '',
                path: sessionStorage.getItem('path'),
                id_user: sessionStorage.getItem('id_user'),
                token: sessionStorage.getItem('token'),
                level: sessionStorage.getItem('level'),
                is_public: is_pub,
                is_folder: '1'
            });

            //Send request to API to register
            this.http.post(Constants.PROTO_API + Constants.DOMAIN_API + 'directory', body, options)
                .subscribe(data => {
                  if (data.text() == "create"){
                    this.nav.setRoot(HomePage);
                  } else {
                    toast.setMessage('Directory already exist !');
                    toast.present();
                    return;
                  }
                }, error => {
                    console.log("Oooops!");
                    return;
                });
        }
    }

    addElementInvalid(element) {
        element.nativeElement.classList.add("ng-invalid");
        element.nativeElement.classList.add("ng-touched");
        element.nativeElement.classList.remove("ng-valid");
    }
}
