import { Component, ViewChild } from "@angular/core";
import { Platform, Nav, Events } from "ionic-angular";
import {  Headers, Http, RequestOptions } from "@angular/http";

import { StatusBar } from '@ionic-native/status-bar';
import { Keyboard } from '@ionic-native/keyboard';
import * as Constants from '../pages/constants';
import { Chart } from 'chart.js';

import { HomePage } from "../pages/home/home";
import { LoginPage } from "../pages/login/login";
import { EditProfilePage } from "../pages/edit-profile/edit-profile";


export interface MenuItem {
    title: string;
    component: any;
    icon: string;
}

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;
  @ViewChild('doughnutCanvas') doughnutCanvas;
  doughnutChart: any;

  name: any;
  username: any;

  rootPage: any = LoginPage;

  appMenuItems: Array<MenuItem>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public keyboard: Keyboard,
    public http: Http,
    public events: Events
  ){
    this.initializeApp();

    events.subscribe(('user:logged'), () => {
      document.getElementById('name').innerHTML = sessionStorage.getItem('firstname') + ' ' + sessionStorage.getItem('lastname');
      document.getElementById('username').innerHTML = sessionStorage.getItem('username');

      let headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
      let options = new RequestOptions({ headers: headers });
      this.http.get(Constants.PROTO_API + Constants.DOMAIN_API + 'directory/' + sessionStorage.getItem('username') + '/?token=' + sessionStorage.getItem('token') + '&id_user=' + sessionStorage.getItem('id_user'), options)
        .subscribe(data => {
          var sizeTotalGo = 30;
          var sizeUsedGo = data.json().size / 1024;
          var sizeRemainingGo = sizeTotalGo - sizeUsedGo;

          this.doughnutChart = new Chart(document.getElementById("doughnutCanvas"), {
            type: 'doughnut',
            data: {
              labels: ["Free", 'Use'],
              datasets: [{
                data: [sizeRemainingGo.toString().substring(0, 5), sizeUsedGo.toString().substring(0, 4)],
                backgroundColor: [
                  "#75C4FD",
                  "#ff0000"
                ]
              }]
            },
            options: {
              legend: {
                display: false
              }
            }
          });
        });

      this.http.get(Constants.PROTO_API + Constants.DOMAIN_API + 'get-folders/?token=' + sessionStorage.getItem('token') + '&id_user=' + sessionStorage.getItem('id_user'))
        .subscribe(data => {
          let result = JSON.parse(data.text());
          var arrayFolder = [];
          arrayFolder.push({title:'Home', component:HomePage, icon:'home', path: sessionStorage.getItem('username')+'/', level: '0'});

          for(var x = 0; x < result.length; x++){
            arrayFolder.push({title: result[x].path.substr(1, result[x].path.length) + result[x].name , component:HomePage, icon:'folder', path: result[x].path.substr(1, result[x].path.length) + result[x].name+'/', level: result[x].level});
          }

          this.appMenuItems = arrayFolder;
        });
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.

      //*** Control Splash Screen
      // this.splashScreen.show();
      // this.splashScreen.hide();

      //*** Control Status Bar
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);

      //*** Control Keyboard
      this.keyboard.disableScroll(true);
    });
  }

  openPage(page) {
    this.nav.setRoot(HomePage, {path: page.path, level: page.level});
  }

  editProfile() {
    this.nav.setRoot(EditProfilePage);
  }

  logout() {
    sessionStorage.clear();
    this.nav.setRoot(LoginPage);
    sessionStorage.clear();
  }
}
